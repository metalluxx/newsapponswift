//
//  ImageCacheController.swift
//  NewsOnSwift
//
//  Created by Metalluxx on 04/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Cocoa

/// Class for caching image from news
class ImageCacheController: NSObject {

    
    private static var sharedPointer : ImageCacheController? = nil
    /// Shared ImageCacheController
    static var shared : ImageCacheController = {
        if sharedPointer == nil {
            sharedPointer = ImageCacheController()
        }
        return sharedPointer!
    }()
    
    
    static var cacheUrl : URL = URL.init(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, false)[0])
    static let imageCacheUrl : URL = cacheUrl.appendingPathComponent("NewsImageCache", isDirectory: true)

    static func getCacheImageLocation(imageName: String) -> String {
        var imageOnFileSystem = (ImageCacheController.imageCacheUrl.appendingPathComponent(imageName, isDirectory: false)).absoluteString
        imageOnFileSystem.removeSubrange(imageOnFileSystem.startIndex...imageOnFileSystem.index(imageOnFileSystem.startIndex, offsetBy: 6))
        return imageOnFileSystem
    }

    
    static var urlSession : URLSession?
    
    override init() {
        super.init()
        ImageCacheController.urlSession = URLSession.init(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: OperationQueue.current)
        
        do{
            try ImageCacheController.imageCacheUrl.checkResourceIsReachable()
        }
        catch{
            try! FileManager.default.createDirectory(at: ImageCacheController.imageCacheUrl, withIntermediateDirectories: true, attributes: nil)
        }
    }
    
    func canceAllDownloadTask() -> Void {
        ImageCacheController.urlSession!.getTasksWithCompletionHandler { (dataTasks, uploadTasks ,downlaodTasks) in
            for task in downlaodTasks{
                task.cancel()
            }
        }
    }
    
    func canceAllDownloadTask( description taskDescription: String?) -> Void {
        if taskDescription != nil {
          ImageCacheController.urlSession!.getTasksWithCompletionHandler { (dataTasks, uploadTasks ,downlaodTasks) in
                for task in downlaodTasks{
                    if (task.taskDescription != nil) && (task.taskDescription == taskDescription!) {
                        task.cancel()
                    }
                }
            }
        }
    }
    

    
    /// Get image from cache or internet
    ///
    /// - Parameters:
    ///   - url: image url for downloading
    ///   - imageComplitionHandler: func execute after getting data
    func downloadDataWithCache(from url: URL?, descriprion: String? ,imageComplitionHandler: @escaping (URL?, URLResponse?, Error?) -> (Void)? ) -> (Void) {
  
        self.canceAllDownloadTask(description: descriprion)
        
        if url != nil {
            let nameFromUrl = url!.pathComponents.last!
            let urlImageOnFileSystem : URL? = URL.init(fileURLWithPath: ImageCacheController.getCacheImageLocation(imageName: nameFromUrl) )
            
            if FileManager.default.isReadableFile(atPath:ImageCacheController.getCacheImageLocation(imageName: nameFromUrl)) {
                imageComplitionHandler(urlImageOnFileSystem, nil, nil)
            }
            else
            {
                let downloadTask : URLSessionDownloadTask! = ImageCacheController.urlSession?.downloadTask(with: url!, completionHandler: {
                    (content, response, error) in
                    
                    if content != nil {
                        try? FileManager.default.copyItem(at: content!, to: urlImageOnFileSystem!)
                        imageComplitionHandler(urlImageOnFileSystem, response, error)
                    }
                    
                } )
                downloadTask.taskDescription = descriprion
                downloadTask.resume()
            }
        }
    }
    
    
}
