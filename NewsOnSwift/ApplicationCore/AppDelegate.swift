//
//  AppDelegate.swift
//  NewsOnSwift
//
//  Created by Metalluxx on 02/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
        try? UserDefaults.standard.set(JSONEncoder().encode(Agregator.shared().sourceList), forKey: "NewsSourceItems")
    }
}


