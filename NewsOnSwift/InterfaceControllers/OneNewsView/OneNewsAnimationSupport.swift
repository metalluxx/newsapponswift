//
//  File.swift
//  NewsOnSwift
//
//  Created by Metalluxx on 03/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation
import QuartzCore

/// Controller CoreAnimation layers ONVC
struct OneNewsCALayers {
    
    var caContentText : CALayer? = nil
    var caImageView : CALayer? = nil
    var caDescription : CALayer? = nil
    var caTitleText : CALayer? = nil
    var caProgressIndicator : CALayer? = nil
    var currentController : OneNewsViewController? = nil
    
    
    init?(for onvc: OneNewsViewController?) {
        if !(onvc == nil) {
            currentController = onvc
            refreshLayers()
        }
        else {
            return nil
        }
    }
    
    /// Update ONVC layers
    mutating func refreshLayers() -> Void {
        if !(currentController == nil) {
            caContentText =  currentController!.otlContentText.layer
            caImageView = currentController!.otlImageView.layer
            caDescription = currentController!.otlDescriptionText.layer
            caTitleText = currentController!.otlTitleText.layer
            caProgressIndicator = currentController!.otlProgressIndicator.layer
        }
    }
}

/// Animation controller ONVC(using OneNewsCALayers)
class OneNewsAnimationController: NSObject {
    
    /// Get fade animation
    static let animationFade : CATransition = { () -> (CATransition) in
        let res : CATransition =  CATransition.init()
        res.duration = 0.2
        res.timingFunction = CAMediaTimingFunction.init(name: CAMediaTimingFunctionName.easeInEaseOut)
        res.type = CATransitionType.fade
        return res
    }()
    
    /// Get moveLeft animation
    static let animationMoveLeft : CATransition = { () ->(CATransition) in
        let res : CATransition = CATransition.init()
        res.duration = 0.2
        res.timingFunction = CAMediaTimingFunction.init(name: CAMediaTimingFunctionName.easeInEaseOut)
        res.type = CATransitionType.moveIn
        res.subtype = CATransitionSubtype.fromLeft
        return res
    }()
    
    /// Start fade animation for ONVC layers
    ///
    /// - Parameter oneNewsVievController: animate ONVC
    static func addAnimation(forONVC oneNewsVievController: OneNewsViewController?) -> Void {
        
        if oneNewsVievController != nil {
            let currentLayers : OneNewsCALayers = OneNewsCALayers.init(for: oneNewsVievController)!
            currentLayers.caContentText?.add(animationFade, forKey: "animatedPosition")
            currentLayers.caProgressIndicator?.add(animationFade, forKey: "animatedPosition")
            currentLayers.caTitleText?.add(animationFade, forKey: "animatedPosition")
            currentLayers.caDescription?.add(animationFade, forKey: "animatedPosition")
            currentLayers.caImageView?.add(animationFade, forKey: "animatedPosition")
        }
    }
}
