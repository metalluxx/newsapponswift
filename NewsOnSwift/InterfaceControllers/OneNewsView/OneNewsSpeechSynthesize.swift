//
//  OneNewsSpeechSynthesize.swift
//  NewsOnSwift
//
//  Created by Metalluxx on 05/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Cocoa

class OneNewsSpeechSynthesize: NSObject {

    static var sharedPointer : OneNewsSpeechSynthesize? = nil
    static var shared : OneNewsSpeechSynthesize = { () -> (OneNewsSpeechSynthesize) in
        if sharedPointer == nil{
           sharedPointer = OneNewsSpeechSynthesize()
        }
        return sharedPointer!
    }()
    
    static var speechController : NSSpeechSynthesizer? = NSSpeechSynthesizer.init()
    
    var delegate : OneNewsSpeechDelegate? = nil
    
    
    override init() {
        super.init()
        OneNewsSpeechSynthesize.speechController!.delegate = self
        OneNewsSpeechSynthesize.speechController!.setVoice(NSSpeechSynthesizer.defaultVoice)
    }
    
    func speechNews(news : OneNewsViewController) -> Void {
        self.delegate?.didFinishSpeaking(sender: self)
        OneNewsSpeechSynthesize.speechController!.stopSpeaking()
        self.delegate = news
        self.delegate?.didStartSpeaking(sender: self)
        OneNewsSpeechSynthesize.speechController!.startSpeaking(news.otlContentText.string)
    }
    
    func removedCurrnentNews() -> Void {
        self.delegate = nil
        OneNewsSpeechSynthesize.speechController?.stopSpeaking()
    }
}


extension OneNewsSpeechSynthesize : NSSpeechSynthesizerDelegate {
    func speechSynthesizer(_ sender: NSSpeechSynthesizer, didFinishSpeaking finishedSpeaking: Bool) {
        
        if delegate != nil {
            if finishedSpeaking{
                delegate!.didFinishSpeaking(sender: self)
                self.delegate = nil
            }
        }
    }
}


protocol OneNewsSpeechDelegate {
    func didStartSpeaking(sender : Any?) -> Void
    func didFinishSpeaking(sender : Any?) -> Void
}
