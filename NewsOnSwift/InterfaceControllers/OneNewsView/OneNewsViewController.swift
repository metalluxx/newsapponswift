//
//  OneNewsViewController.swift
//  NewsOnSwift
//
//  Created by Metalluxx on 02/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Cocoa

/// View controller for showing news from OneNews
class OneNewsViewController: NSViewController {

    @IBOutlet weak var otlSpeechButton: NSButton!
    @IBOutlet var otlView: NSView!
    @IBOutlet weak var otlImageView: NSImageView!
    @IBOutlet weak var otlTitleText: NSTextField!
    @IBOutlet weak var otlDescriptionText: NSTextField!
    @IBOutlet weak var otlProgressIndicator: NSProgressIndicator!
    @IBOutlet weak var otlBrowserButton: NSButton!
    @IBOutlet var otlContentText: NSTextView!
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addObserver(self, forKeyPath: "news", options: [], context: nil)
        self.didUpdateCurrentNews(sender: nil)
    }
    
    
    let id : UInt32 = arc4random()
    
    @objc dynamic var news : OneNews?
    
    var oneNewsDownloadTask : URLSessionDownloadTask? = nil
    

    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "news" {
            self.didUpdateCurrentNews(sender: self)
        }
        else{
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }

    
    /// Enameration GUI states for ONVC
    ///
    /// - loadingImage: state, while loading image news, correct only imageView
    /// - field: while have current OneNews in Agregator
    /// - empty: while dont have current OneNews
    enum OneNewsInterfaceControl {
        case loadingImage
        case loadedImage
        case field
        case empty
    }
    
    /// Control outlets status for another situations
    ///
    /// - Parameter state: current situation
    
    func controlInterface(state: OneNewsInterfaceControl) -> Void {
        if state == OneNewsInterfaceControl.empty {
            otlImageView.image = nil
            otlBrowserButton.isEnabled = false
            otlSpeechButton.isEnabled = false
            
            otlDescriptionText.stringValue = ""
            otlDescriptionText.isBordered = false
            
            otlTitleText.stringValue = ""
            otlContentText.string = ""
            
            otlProgressIndicator.stopAnimation(self)
            otlProgressIndicator.isHidden = true
        }
            
        else if state == OneNewsInterfaceControl.field {
            otlBrowserButton.isEnabled = true
            otlSpeechButton.isEnabled = true
            otlTitleText.stringValue = self.news!.title
            
            otlDescriptionText.isBordered = true
            otlDescriptionText.stringValue = self.news!.descriptions
            
            otlContentText.string = self.news!.content
            otlImageView.image = nil
        }
        else if state == OneNewsInterfaceControl.loadingImage {
            otlProgressIndicator.isHidden = false
            otlProgressIndicator.startAnimation(self)
        }
        else if state == OneNewsInterfaceControl.loadedImage {
            self.otlProgressIndicator.isHidden = true
            self.otlProgressIndicator.stopAnimation(self)
        }
    }
    

    /// Executable func after download news image in ONVC
    ///
    /// - Parameters:
    ///   - content: from completionHandler
    ///   - response: from completionHandler
    ///   - error: from completionHandler
    func completionHandlerForLoadedImage(content : URL?, response : URLResponse?, error : Error?) -> Void {
        do{
            if content == nil {
                throw error!
            }
            
            let binaryImage : Data? = try Data.init(contentsOf: content!)
            let downLoadedImage : NSImage? = NSImage.init(data: binaryImage!)
            
            DispatchQueue.main.async {
                self.otlImageView.image = downLoadedImage!
                self.controlInterface(state: .loadedImage)
                OneNewsCALayers.init(for: self)?.caImageView?.add(OneNewsAnimationController.animationMoveLeft, forKey: "animatedPosition")
                self.oneNewsDownloadTask = nil
            }
        }
        catch{
            print(error)
        }
    }

    
    
    /// Update ONVC fields after changing currentNews in Agregator(this func exec from KVO)
    ///
    /// - Parameter sender: sended object
    
    func didUpdateCurrentNews (sender : Any?) -> Void {
        
        OneNewsSpeechSynthesize.shared.removedCurrnentNews()
        
        if oneNewsDownloadTask != nil {
            oneNewsDownloadTask?.cancel()
            oneNewsDownloadTask = nil
        }
        
        // clean UI
        if self.news == nil {
            controlInterface(state: .empty)
            controlInterface(state: .loadedImage)
        } else {
            controlInterface(state: .field)
            controlInterface(state: .loadedImage)
            
            if self.news!.urlToImage != nil{
                if self.news!.urlToImage!.absoluteString != ""
                {
                    controlInterface(state: .loadingImage)
                    let imageUrl = self.news!.urlToImage!
                    ImageCacheController.shared.downloadDataWithCache(from: imageUrl, descriprion: "OnvcImageDonload-\(String(self.id))" , imageComplitionHandler: completionHandlerForLoadedImage(content:response:error:))
                }
            }
        }
        OneNewsAnimationController.addAnimation(forONVC: self)
        
    }
    
    @IBAction func actSpeechButton(_ sender: Any) {
        OneNewsSpeechSynthesize.shared.speechNews(news: self)
    }
    
    /// Clicked browser button
    ///
    /// - Parameter sender: obj sender
    @IBAction func actBrowserButtonClicked(_ sender: Any) {
        NSWorkspace.shared.open((news!.url)!)
    }
}



extension OneNewsViewController : OneNewsSpeechDelegate {
    func didStartSpeaking(sender: Any?) {
        self.otlSpeechButton.isEnabled = false
    }
    
    func didFinishSpeaking(sender: Any?) {
        self.otlSpeechButton.isEnabled = true
    }
}
