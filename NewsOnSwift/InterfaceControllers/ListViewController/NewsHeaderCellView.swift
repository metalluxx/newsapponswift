//
//  NewsHeaderCellView.swift
//  NewsOnSwift
//
//  Created by Metalluxx on 02/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Cocoa

class NewsHeaderCellView: NSTableCellView {

    @IBOutlet weak var otlHeaderText: NSTextField!
    @IBOutlet weak var otlCountNews: NSTextField!
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        
        // Drawing code here.
    }
    
    override func viewWillDraw() {
        super.viewWillDraw()
        otlCountNews.sizeToFit()
    }
    

}
