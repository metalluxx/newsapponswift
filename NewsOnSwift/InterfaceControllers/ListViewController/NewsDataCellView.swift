//
//  NewsDataCellView.swift
//  NewsOnSwift
//
//  Created by Metalluxx on 06/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Cocoa

class NewsDataCellView: NSTableCellView {

    @IBOutlet weak var otlDataText: NSTextField!
    @IBOutlet weak var otlSourceText: NSTextField!
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)

        // Drawing code here.
    }

    override func viewWillDraw() {
        super.viewWillDraw()
        otlSourceText.sizeToFit()
    }
    
}
