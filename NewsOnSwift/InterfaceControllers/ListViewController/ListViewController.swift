//
//  ListViewController.swift
//  NewsOnSwift
//
//  Created by Metalluxx on 02/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Cocoa


protocol ListViewControllerDelegate {
    func selectedItemInOutlineView(news : OneNews?)
}


class ListViewController: NSViewController {
    
    @IBOutlet var otlView: NSView!
    @IBOutlet weak var otlOutlineView: NSOutlineView!
    @IBOutlet weak var otlTableColumn: NSTableColumn!
    
    var delegate : ListViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        otlOutlineView.register(NSNib.init(nibNamed: "NewsDataCellView", bundle: nil), forIdentifier: NSUserInterfaceItemIdentifier(rawValue: "DataCell"))
        Agregator.shared().delegate = self;
        Agregator.shared().loadNews()
        otlOutlineView.doubleAction = #selector(doubleClickedOutlineViewItem)
        
        // Do view setup here.
    }
    
    @objc func doubleClickedOutlineViewItem() -> Void {
        if otlOutlineView.item(atRow: otlOutlineView.selectedRow) is OneNews {
            let storyboard = NSStoryboard.init(name: "Main", bundle: nil)
            
            let oneNewsController = storyboard.instantiateController(withIdentifier: "OneNewsSID") as! OneNewsViewController
            oneNewsController.news = otlOutlineView.item(atRow: otlOutlineView.selectedRow) as? OneNews
            
            let window = NSWindow.init(contentViewController: oneNewsController as NSViewController)
            window.title = (oneNewsController.news?.title)!
            window.titlebarAppearsTransparent = true
            window.titleVisibility = .hidden
            window.styleMask = [.borderless, .titled, .closable, .miniaturizable, .resizable, .fullSizeContentView]
            
            let windowController = NSWindowController.init(window: window)
            windowController.showWindow(nil)
        }
    }
}

extension ListViewController : NSOutlineViewDataSource, NSOutlineViewDelegate
{
    func outlineView(_ outlineView: NSOutlineView, numberOfChildrenOfItem item: Any?) -> Int {
        if item == nil {
            return Agregator.shared().arrayGroupsNews.count
        }
        else if item is GroupNews {
            let group = item as! GroupNews
            return group.news.count
        }
        return 0
    }
    
    func outlineView(_ outlineView: NSOutlineView, child index: Int, ofItem item: Any?) -> Any {
        if item == nil {
            return Agregator.shared().arrayGroupsNews[index]
        }
        else{
            let group = item as! GroupNews
            return group.news[index]
        }
    }
    func outlineView(_ outlineView: NSOutlineView, isItemExpandable item: Any) -> Bool {
        if item is OneNews{
            return false
        }
        return true
    }
    
    func outlineView(_ outlineView: NSOutlineView, viewFor tableColumn: NSTableColumn?, item: Any) -> NSView? {
        if item is OneNews {
            let currentNews = item as! OneNews
            let dataCell : NewsDataCellView = outlineView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "DataCell"), owner: self) as! NewsDataCellView
            dataCell.otlDataText.stringValue = currentNews.title
            dataCell.otlSourceText.stringValue = (currentNews.source != "") ? currentNews.source : "Other"
            return dataCell
        }
        else {
            let currentGroupNews : GroupNews = item as! GroupNews
            let headerCell : NewsHeaderCellView = outlineView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "HeaderCell"), owner: self) as! NewsHeaderCellView
            headerCell.otlHeaderText.stringValue = currentGroupNews.name
            headerCell.otlCountNews.stringValue = "\(currentGroupNews.news.count) news"
            return headerCell
        }
    }
    
    func outlineViewSelectionDidChange(_ notification: Notification) {
        let currentItem : Any = otlOutlineView.item(atRow: otlOutlineView.selectedRow) as Any
        
        if delegate != nil {
            
            if currentItem is OneNews  {
                delegate!.selectedItemInOutlineView(news: (currentItem as! OneNews))
            }
            else{
                delegate!.selectedItemInOutlineView(news: nil)
            }
            
        }
    }
    func outlineView(_ outlineView: NSOutlineView, heightOfRowByItem item: Any) -> CGFloat {
        if item is OneNews {
            return 40.0
        }
        else {
            return 20.0
        }
    }
}
    



extension ListViewController : AgregatorDelegate {
    
    func didLoadNewsFromServer(sender: Any, sourceItem: SourceItem?) {
        if !(sourceItem == nil) {
            DispatchQueue.main.async {
                self.otlOutlineView.reloadData()
            }
        }
    }
}


