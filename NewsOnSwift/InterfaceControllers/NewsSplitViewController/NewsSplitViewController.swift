//
//  NewsSplitViewController.swift
//  NewsOnSwift
//
//  Created by Metalluxx on 05/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Cocoa

class NewsSplitViewController: NSSplitViewController, ListViewControllerDelegate{
    func selectedItemInOutlineView(news: OneNews?) {
        oneNewsViewController?.news = news
    }
    
    var oneNewsViewController : OneNewsViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    override func addSplitViewItem(_ splitViewItem: NSSplitViewItem) {
        super.addSplitViewItem(splitViewItem)
        if splitViewItem.viewController is ListViewController {
            let findedView = splitViewItem.viewController as? ListViewController
            findedView?.delegate = self
        }
        if splitViewItem.viewController is OneNewsViewController {
            self.oneNewsViewController = splitViewItem.viewController as? OneNewsViewController
        }
    }
}


