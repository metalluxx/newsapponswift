//
//  SourceItemView.swift/Users/metalluxx/newsapponswift/NewsOnSwift/InterfaceControllers/PreferencesViewController/PreferencesViewController.swift
//  NewsOnSwift
//
//  Created by Metalluxx on 06/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Cocoa

class SourceItemView: NSTableCellView {

    @IBOutlet weak var otlEnabledCheckBox: NSButton!
    @IBOutlet weak var otlNameText: NSTextField!
    @IBOutlet weak var otlSourceUrlText: NSTextField!
    var sourceItem : SourceItem?
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)

        // Drawing code here.
    }
    
}
