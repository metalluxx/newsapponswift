//
//  PreferencesViewController.swift
//  NewsOnSwift
//
//  Created by Metalluxx on 06/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Cocoa

class PreferencesViewController: NSViewController {

    var sourceList : [SourceItem] = Agregator.shared().sourceList
    
    
    
    @IBOutlet weak var otlTableView: NSTableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.window?.delegate = self
        // Do view setup here.
    }
    
    @IBAction func actAddSource(_ sender: Any) {
        var newSource = SourceItem()
        sourceList.append(newSource)
        otlTableView.reloadData()
        Agregator.shared().reloadData()
        
    }
    @IBAction func actDeleteSource(_ sender: Any) {
        
    }
    @IBAction func actReloadData(_ sender: Any) {
        
    }
}


extension PreferencesViewController : NSTableViewDelegate, NSTableViewDataSource{
    func tableView(_ tableView: NSTableView, objectValueFor tableColumn: NSTableColumn?, row: Int) -> Any? {
        let view : SourceItemView = SourceItemView()
        view.sourceItem = sourceList[row]
        return view
    }

    func numberOfRows(in tableView: NSTableView) -> Int {
        return Agregator.shared().sourceList.count
    }
}

extension PreferencesViewController : NSWindowDelegate{
    
}
