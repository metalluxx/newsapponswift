//
//  WindowController.swift
//  NewsOnSwift
//
//  Created by Metalluxx on 02/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Cocoa

class WindowController: NSWindowController, NSWindowDelegate {

    @IBOutlet weak var otlWindow: NSWindow!
    override func windowDidLoad() {
        super.windowDidLoad()
        

    
        // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    }

    func windowWillClose(_ notification: Notification) {
        NSApp.terminate(self)
    }
}
