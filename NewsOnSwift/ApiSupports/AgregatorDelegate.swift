//
//  AgregatorDelegateProtocol.swift
//  NewsOnSwift
//
//  Created by Metalluxx on 02/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation


/// Agregator delegate protocol for action aftel loaded data from server
protocol AgregatorDelegate : class {
    ///  Call func after loaded one SourceItem
    ///
    /// - Parameters:
    ///   - sender: called object this func
    ///   - sourceItem: SourceItem loaded
    /// - Returns: Nothing
    func didLoadNewsFromServer(sender : Any, sourceItem: SourceItem?) -> Void
}
