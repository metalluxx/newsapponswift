//
//  OneNews.swift
//  NewsOnSwift
//
//  Created by Metalluxx on 02/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Cocoa


/*
 @property NSString* title;
 @property NSString* author;
 @property NSString* content;
 @property NSString* description;
 @property NSString* publishedAt;
 @property NSURL* url;
 @property NSURL* urlToImage;
 @property NSString* source;
 @property NSString* appSource;
 - (instancetype)initWithDictonary: (NSDictionary*) dic;
 */

/*
 if([dic valueForKey:@"url"] == [NSNull null]) self.url = [NSURL URLWithString:@""];
 else self.url = [NSURL URLWithString:[dic valueForKey:@"url"]];
 
 if([dic valueForKey:@"urlToImage"] == [NSNull null]) self.url = [NSURL URLWithString:@""];
 else self.urlToImage = [NSURL URLWithString:[dic valueForKey:@"urlToImage"]];
 
 self.author = [dic valueForKey:@"author"];
 self.content = [dic valueForKey:@"content"];
 self.title = [dic valueForKey:@"title"];
 self.publishedAt = [dic valueForKey:@"publishedAt"];
 self.description = [dic valueForKey:@"description"];
 
 NSDictionary* binSource = [dic valueForKey:@"source"];
 self.source = [binSource valueForKey:@"name"];
 */


class OneNews : NSObject {
    public var title : String = ""
    public var author : String = ""
    public var content : String = ""
    public var descriptions : String = ""
    public var publishedAt : String = ""
    public var url : URL? = nil
    public var urlToImage : URL? = nil
    public var source : String = ""
    public var appSource : String = ""
    
    
    init(dictonary: NSDictionary?) {
        if !(dictonary == nil) {
            
            let strUrl : String? = dictonary!.value(forKey: "url") as? String
            if !(strUrl == nil){
                if strUrl != ""{
                    self.url = URL.init(string: strUrl!)
                }
            }
            
            
            let strUrlToImage : String? = dictonary!.value(forKey: "urlToImage") as? String
            if !(strUrlToImage == nil){
                if strUrlToImage != ""{
                    self.urlToImage = URL.init(string: strUrlToImage!)
                }
            }
            
            self.author = (dictonary!["author"] is NSNull) ? "" : dictonary!["author"] as! String 
            self.content = (dictonary!["content"] is NSNull) ? "" : dictonary!["content"] as! String
            self.title = (dictonary!["title"] is NSNull) ? "" : dictonary!["title"] as! String
            self.publishedAt = (dictonary!["publishedAt"] is NSNull) ? "" : dictonary!["publishedAt"] as! String
            self.descriptions = (dictonary!["description"] is NSNull) ? "" : dictonary!["description"] as! String
            var binSource : Dictionary! = dictonary!["source"] as? [String:String]
            self.source = binSource?["name"] ?? ""
        }
    }
}




