//
//  Agregator.swift
//  NewsOnSwift
//
//  Created by Metalluxx on 02/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Cocoa

class Agregator: NSObject {
    private static var sharedPointer : Agregator?
    var delegate : AgregatorDelegate? = nil
    var sourceList : Array<SourceItem>!
    var arrayGroupsNews: Array<GroupNews>
    
    private override init() {
        sourceList = Array<SourceItem>()
        arrayGroupsNews = Array<GroupNews>()
    
//        let defOneSource : SourceItem = SourceItem.init()
//        defOneSource.name = "Apple"
//        defOneSource.url = URL.init(string: "https://newsapi.org/v2/everything?q=apple&from=2019-03-01&to=2019-03-01&sortBy=popularity&apiKey=344b923128d14c118f7a96a5564a16cb")!
//        defOneSource.enabled = true
//
//        sourceList.append(defOneSource)
        
        if UserDefaults.standard.object(forKey: "NewsSourceItems") == nil {
            let defOneSource : SourceItem = SourceItem.init()
            defOneSource.name = "Apple"
            defOneSource.url = URL.init(string: "https://newsapi.org/v2/everything?q=apple&from=2019-02-16&to=2019-02-16&sortBy=popularity&apiKey=344b923128d14c118f7a96a5564a16cb")!
            defOneSource.enabled = true
            
            var defArray : Array<SourceItem> = Array<SourceItem>()
            defArray.append(defOneSource)
            try? UserDefaults.standard.set(JSONEncoder().encode(defArray), forKey: "NewsSourceItems")
        }
        sourceList = try? JSONDecoder().decode(Array<SourceItem>.self, from: UserDefaults.standard.object(forKey: "NewsSourceItems") as! Data)
    }
    

    /// Get shared "Agregator" for application
    ///
    /// - Returns: Shared object
    static func shared() -> (Agregator) {
        if sharedPointer == nil {
            sharedPointer = Agregator()
        }
        return sharedPointer!
    }
    
    
    func reloadData() -> Void {
        arrayGroupsNews.removeAll()
        delegate?.didLoadNewsFromServer(sender: self, sourceItem: nil)
        loadNews()
    }
    
    /// Loading news from SourceItem array
    func loadNews() -> Void {
        for oneSrcItem in sourceList {
            if oneSrcItem.enabled {
                    let downloadTask : URLSessionDownloadTask =  URLSession.shared.downloadTask(with: oneSrcItem.url!) { (location: URL?, _, _) in
                        if !(location == nil){
                            let binaryFileJson : Data? =  try! Data.init(contentsOf: location!)
                            self.parseJSON(binaryJson: binaryFileJson!, forSourceItem: oneSrcItem)
                        }
                    }
                    downloadTask.resume()
            }
        }
    }
    
    /// Parsing JSON for current SourceItem and feel news array
    ///
    /// - Parameters:
    ///   - binJson: JSON from one source
    ///   - srcItem: SourceItem information for correct field GroupNews
    func parseJSON(binaryJson binJson: Data, forSourceItem srcItem: SourceItem) -> Void {
        
        var json : [String: AnyObject] = try! JSONSerialization.jsonObject(with: binJson, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
        var oneGroup : GroupNews = GroupNews()
        oneGroup.name = srcItem.name
        let news : Array<[String:AnyObject]> = json["articles"] as! Array<[String:AnyObject]>
        
        for newsDic in news {
            let oneNews : OneNews = OneNews.init(dictonary: newsDic as NSDictionary)
            oneGroup.news.append(oneNews)
        }
        
        arrayGroupsNews.append(oneGroup)
        delegate?.didLoadNewsFromServer(sender: self, sourceItem: srcItem)
    }
}
