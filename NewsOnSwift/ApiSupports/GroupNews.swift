//
//  GroupNews.swift
//  NewsOnSwift
//
//  Created by Metalluxx on 02/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation


struct GroupNews {
    var name : String = ""
    var news : Array<OneNews> = Array<OneNews>()
}
