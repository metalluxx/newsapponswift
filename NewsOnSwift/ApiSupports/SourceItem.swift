//
//  SourceItem.swift
//  NewsOnSwift
//
//  Created by Metalluxx on 02/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Cocoa

class SourceItem : NSObject, Codable {
    var url : URL?
    var name : String = ""
    var enabled : Bool = false
    
    func encode(with aCoder: NSCoder) {
        
        self.url == nil ? aCoder.encode("", forKey: "url") : aCoder.encode(self.url?.absoluteString, forKey: "url")
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.enabled, forKey: "enabled")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        self.init()
        let tmpUrl = aDecoder.decodeObject(forKey: "url") as! String
        tmpUrl == "" ?(self.url = nil): (self.url = URL.init(string: tmpUrl as! String))
        self.name = aDecoder.decodeObject(forKey: "name") as! String
        self.enabled = aDecoder.decodeObject(forKey: "enabled") as! Bool
    }
}


extension SourceItem : NSCopying {
    @objc func copy(with zone: NSZone? = nil) -> Any {
        let cpy : Data = NSKeyedArchiver.archivedData(withRootObject: self) as Data
        return NSKeyedUnarchiver.unarchiveObject(with: cpy as Data) as! SourceItem
    }
}

